# Autofill.me

Autofill.me is an innovative testing platform specifically designed for evaluating browser-based form-filling extensions and scripts. It offers a range of form scenarios, enabling developers to thoroughly test autofill functionalities under various conditions!

## Getting Started

Follow these steps to set up Autofill.me for development and testing on your local machine.

### Prerequisites

Ensure you have the following installed:

- **Node.js**: Required for running the web server and backend. Download from [Node.js official website](https://nodejs.org/).
- **Playwright**: Used for running end-to-end tests. Installation and setup details are available in the [Playwright Documentation](https://playwright.dev/docs/intro).

### Installation and Maintenance

To install Autofill.me:

1. In `e2e`, run `pnpm install` to install Node.js dependencies.
2. If you need to maintain or expand on the project, it's inside the `server` folder
3. Write your tests on the `tests` folder. Add the line `test.use({ baseURL: "http://localhost:3000" });` before your test

## Hosting Environment

Autofill.me is hosted on AWS Elastic Beanstalk, providing a robust and scalable environment for the application.

### Current Deployment Process

As of now, the deployment to AWS Elastic Beanstalk is a manual process. It involves packaging the application and uploading it to the Elastic Beanstalk environment through the AWS Management Console.

### Future Plans for Deployment Automation

We aim to automate this deployment process in the future. The goal is to implement a CI/CD pipeline that will automatically handle the deployment of new versions of Autofill.me to AWS Elastic Beanstalk, ensuring faster and more efficient updates to the platform.

## Features

- **Diverse Form Scenarios**: A variety of forms to test different autofill scripts.
- **Customizable Testing**: Ability to modify and create form scenarios for specific tests.
- **Real-World Form Emulation**: Mimics real-world forms for accurate testing results.

## Using Autofill.me

Autofill.me is designed for both automated and manual testing of form-filling capabilities in diverse scenarios. It’s an ideal environment to test, refine, and validate your autofill solutions.
