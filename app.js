const crypto = require("crypto");
const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const app = express();

function generateChallenge() {
  return crypto.randomBytes(32).toString("base64");
}

app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "public"))); // Serve static files

app.get("/form/:page", (req, res) => {
  const page = req.params.page;
  res.sendFile(path.join(__dirname, `public/form/${page}.html`), (err) => {
    if (err) {
      // If the specific HTML file does not exist, send a 404 error or redirect
      res.status(404).send("Page not found");
    }
  });
});

app.get("/save/:page", (req, res) => {
  const page = req.params.page;
  res.sendFile(path.join(__dirname, `public/save/${page}.html`), (err) => {
    if (err) {
      // If the specific HTML file does not exist, send a 404 error or redirect
      res.status(404).send("Page not found");
    }
  });
});

app.use(bodyParser.json());
app.use(express.static("public")); // Serve static files from the 'public' directory

// Placeholder for storing user data - use a database in production
const users = {};

app.post("/register-challenge", (req, res) => {
  const challenge = generateChallenge();
  // Store the challenge temporarily, associated with the user's email
  const { email } = req.body;
  users[email] = { challenge };
  res.json({ challenge });
});

app.post("/register", (req, res) => {
  const { email, credential } = req.body;
  if (users[email] && users[email].challenge === credential.challenge) {
    users[email].credential = credential;
    res.json({ status: "success" });
  } else {
    res.json({ status: "error", message: "Registration failed" });
  }
});

app.post("/login-challenge", (req, res) => {
  const challenge = generateChallenge();
  const { email } = req.body;
  if (users[email]) {
    users[email].challenge = challenge;
    res.json({ challenge });
  } else {
    res.json({ status: "error", message: "User not found" });
  }
});

app.post("/login", (req, res) => {
  const { email, credential } = req.body;
  if (users[email] && users[email].credential === credential) {
    res.json({ status: "success" });
  } else {
    res.json({ status: "error", message: "Authentication failed" });
  }
});

app.post("/submit", (req, res) => {
  res.json({ redirect: "/form/submit", data: req.body });
});

module.exports = app;
