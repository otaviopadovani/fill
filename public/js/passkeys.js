// Register with Passkey
document
	.getElementById("register-button")
	.addEventListener("click", async () => {
		try {
			const email = document.getElementById("email").value;
			const response = await fetch("/register-challenge", {
				method: "POST",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify({ email }),
			});
			const { challenge } = await response.json();
			const decodedChallenge = Uint8Array.from(atob(challenge), (c) =>
				c.charCodeAt(0),
			);

			const userId = "someUniqueUserId"; // Replace with actual user ID
			const userIdBytes = new TextEncoder().encode(userId);

			const publicKey = {
				challenge: decodedChallenge,
				rp: { name: "Example RP" },
				user: {
					id: userIdBytes,
					name: document.getElementById("email").value,
					displayName: "Username",
				},
				pubKeyCredParams: [{ alg: -7, type: "public-key" }],
			};

			console.log(publicKey);

			const credential = await navigator.credentials.create({
				publicKey,
			});

			await fetch("/register-challenge", {
				method: "POST",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify({ email }),
			});
			alert("Registration successful");
		} catch (error) {
			console.error(error);
			alert("Registration failed");
		}
	});

// Login with Passkey
document.getElementById("login-button").addEventListener("click", async () => {
	try {
		const email = document.getElementById("email").value;
		const response = await fetch("/login-challenge", {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({ email }),
		});
		const { challenge } = await response.json();
		const decodedChallenge = Uint8Array.from(atob(challenge), (c) =>
			c.charCodeAt(0),
		);

		const publicKey = {
			challenge: decodedChallenge,
		};

		const assertion = await navigator.credentials.get({ publicKey });

		// Send the assertion to server for verification
		await fetch("/login", {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(assertion),
		});
		alert("Login successful");
	} catch (error) {
		console.error(error);
		alert("Login failed");
	}
});
