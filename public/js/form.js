document
	.getElementsByTagName("form")[0]
	.addEventListener("submit", function (event) {
		// Prevent the default form submission
		event.preventDefault();

		// Get all the form elements and construct an object to hold the data
		var formElements = this.elements;
		var formData = {};
		for (var i = 0; i < formElements.length; i++) {
			var element = formElements[i];
			if (element.name && element.name !== "_token") {
				// Skip the _token field
				formData[element.name] = element.value;
			}
		}

		// Save the form data in localStorage as a string
		localStorage.setItem("formData", JSON.stringify(formData));

		// Proceed with the form submission
		this.submit();
	});
