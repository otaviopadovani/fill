document
	.getElementsByTagName("form")[0]
	.addEventListener("submit", function (event) {
		event.preventDefault();

		fetch("/submit", {
			method: "POST",
			headers: { "Content-Type": "application/x-www-form-urlencoded" },
			body: new URLSearchParams(new FormData(this)),
		})
			.then((response) => response.json())
			.then((result) => {
				// Store the response data in localStorage or sessionStorage
				sessionStorage.setItem("formData", JSON.stringify(result.data));

				// Redirect to the new page
				window.location.href = result.redirect;
			});
	});
