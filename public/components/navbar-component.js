class NavbarComponent extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({ mode: "open" });
	}

	connectedCallback() {
		this.shadowRoot.innerHTML = `
        <style>
          .navbar-brand {
            display: inline-block;
            padding-top: 0.32rem;
            padding-bottom: 0.32rem;
            margin-right: 1rem;
            font-size: 1.125rem;
            line-height: inherit;
            text-decoration: none;
            color: rgba(0,0,0,.9);
          }
          .navbar {
            padding: 0.5rem 1rem;
            background-color: #fff;
            box-shadow: 0 2px 4px rgba(0,0,0,.1);
            position: relative;
            z-index: 2;
            text-align: center;
          }
          .container {
            display: inline-block;
          }
          .nav-item {
            list-style-type: none;
            position: relative;
            text-align: left;
          }
          .navbar-nav {
            display: inline-block;
            padding: 0;
            margin: 0;
          }
          .navbar-nav li {
            display: inline-block;
          }
          .nav-link {
            display: inline-block;
            text-decoration: none;
            width: 100%;
            white-space: nowrap;
            padding: 0.5rem;
            color: rgba(0,0,0,.9);
          }
          .nav-link em {
            display: inline-block;
            text-decoration: none;
            width: 100%;
            font-style: normal;
            font-size: 0.55rem;
            vertical-align: middle;
          }
          .nav-link:hover {
            color: #0056b3;
          }
          .dropdown-menu {
            display: none;
            position: absolute;
            background-color: #f8f9fa;
            border-radius: 0.25rem;
            left: 0;
            top: 100%;
            border: 1px solid gray;
          }
          .nav-item:hover .dropdown-menu {
            display: block;
          }
        </style>

        <nav class="navbar">
          <div class="container">
            <a class="navbar-brand" href="/">Test Autofill</a>
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#">Login <em>▼</em></a>
                <div class="dropdown-menu">
                  <a class="nav-link" href="/form/login-simple">Simple</a>
                  <a class="nav-link" href="https://basic.fill.dev/">HTTP Basic Auth</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#">Registration <em>▼</em></a>
                <div class="dropdown-menu">
                  <a class="nav-link" href="/form/registration-email">Email</a>
                  <a class="nav-link" href="/form/registration-username">Username</a>
                  <a class="nav-link" href="/form/registration-simple">Email and Username</a>
                  <a class="nav-link" href="/form/registration-2fa">TOTP QR</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#">Credit Card <em>▼</em></a>
                <div class="dropdown-menu">
                  <a class="nav-link" href="/form/credit-card-simple">Simple</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#">Identity <em>▼</em></a>
                <div class="dropdown-menu">
                  <a class="nav-link" href="/form/identity-simple">Simple</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#">Avoid <em>▼</em></a>
                <div class="dropdown-menu">
                  <a class="nav-link" href="/form/search">Search</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#">Passkeys <em>▼</em></a>
                <div class="dropdown-menu">
                  <a class="nav-link" href="/form/passkeys">Register and Authenticate</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#">Save in 1Password <em>▼</em></a>
                <div class="dropdown-menu">
                  <a class="nav-link" href="/save/login">Login</a>
                  <a class="nav-link" href="/save/credit-card">Credit Card</a>
                  <a class="nav-link" href="/save/identity">Identity</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#">References <em>▼</em></a>
                <div class="dropdown-menu">
                  <a class="nav-link" href="https://fill.dev/reference/whatwg">HTML Autofill Spec</a>
                  <a class="nav-link" href="https://developer.1password.com/docs/web/compatible-website-design/">1Password Developer Guide</a>
                </div>
              </li>
            </ul>
          </div>
        </nav>
      `;
	}
}

customElements.define("navbar-component", NavbarComponent);
